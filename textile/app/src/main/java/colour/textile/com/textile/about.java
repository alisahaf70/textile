package colour.textile.com.textile;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class about extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Button send =(Button)(findViewById(R.id.send));
        send.requestFocus();
        final EditText topic=(EditText)(findViewById(R.id.topic));
        final EditText name=(EditText)(findViewById(R.id.name));
        final EditText message=(EditText)(findViewById(R.id.message));
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","info@nikriss.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, topic.getText().toString());
                emailIntent.putExtra(Intent.EXTRA_TEXT, "نام: "+name.getText().toString()+"\n"+message.getText().toString());
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });
    }
}
