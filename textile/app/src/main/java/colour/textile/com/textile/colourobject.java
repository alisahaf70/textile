package colour.textile.com.textile;

/**
 * Created by Alireza on 03/06/2017.
 */

public class colourobject {
    public int id;
    public String name;
    public String hex;
    public String code;
    public String Description;
    public int Available;
    public colourobject(int _id,String _name,String _hex,String _code,String _Description,int _Available){
        id=_id;
        name=_name;
        hex=_hex;
        code=_code;
        Description=_Description;
        Available=_Available;
    }
}
