package colour.textile.com.textile;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;

import android.media.MediaActionSound;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.Bitmap;
import android.util.TypedValue;
import android.view.Gravity;

import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
import de.hdodenhof.circleimageview.CircleImageView;

public class ColourGallery extends AppCompatActivity {

    /** Duration of wait **/
    private LinearLayout list;
    /** Called when the activity is first created. */
    @Override
	public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_colour_gallery);
        list = (LinearLayout) findViewById(R.id.list);
        final DataBaseHelper db=DataBaseHelper.getInstance(this);

        ImageButton Searchbtn=(ImageButton)(findViewById(R.id.serachbtn));
        final EditText searchtext=(EditText)(findViewById(R.id.searchtext));
        Searchbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<colourobject> sq=db.SearchColour(searchtext.getText().toString());
                PrepareElements(sq);
            }
        });
        String last = getIntent().getStringExtra("last");
        ArrayList<colourobject> color=null;
        if (last.equals("camera")) {
            color = db.cameracolor;
            MediaActionSound sound = new MediaActionSound();
            sound.play(MediaActionSound.SHUTTER_CLICK);
        }
        else
            color=db.ReadAllColour();
        PrepareElements(color);
    }
    private void PrepareElements(ArrayList<colourobject> color)
    {
        list.removeAllViews();
        int i=0;
        for (i=0;i<color.size()/3;i++)
        {
            SetupElements(color.get(i*3),color.get(i*3+1),color.get(i*3+2));
        }

        if (color.size()-i*3==2)
            SetupElements(color.get(i*3),color.get(i*3+1),null);
        else if (color.size()-i*3==1)
            SetupElements(color.get(i*3),null,null);
    }
    private void SetupElements(colourobject first,colourobject second,colourobject third)
    {
        final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
        RelativeLayout lnl=new RelativeLayout(this);
        RelativeLayout.LayoutParams mainlayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, new GridView.LayoutParams(65, (int) (140 * scale + 0.5f)).height);
        lnl.setLayoutParams(mainlayout);
        //lnl.setBackgroundColor(Color.rgb(215,215,215));


        RelativeLayout rl1=MakeColorElement(first);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(new GridView.LayoutParams((int) (100 * scale + 0.5f),0).width, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        layoutParams.setMargins(0, 0, (int)(7 * scale + 0.5f), 0);
        rl1.setLayoutParams(layoutParams);
        lnl.addView(rl1);
 if (second!=null) {
        RelativeLayout rl2=MakeColorElement(second);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(new GridView.LayoutParams((int) (100 * scale + 0.5f),0).width, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams2.addRule(RelativeLayout.CENTER_IN_PARENT);
        layoutParams2.setMargins(0, 0, (int)(0 * scale + 0.5f), 0);
        rl2.setLayoutParams(layoutParams2);
        lnl.addView(rl2);
 if (third!=null) {
        RelativeLayout rl3=MakeColorElement(third);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(new GridView.LayoutParams((int) (100 * scale + 0.5f),0).width, RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams3.addRule(RelativeLayout.ALIGN_PARENT_START);
        layoutParams3.setMargins((int)(7 * scale + 0.5f), 0, 0, 0);
        rl3.setLayoutParams(layoutParams3);
        lnl.addView(rl3);
 }
 }

        list.addView(lnl);
        LinearLayout barimg=new LinearLayout(this);
        //barimg.setBackgroundColor(Color.rgb (215,215,215));
        RelativeLayout.LayoutParams layoutParamsbarimg=new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, new GridView.LayoutParams(0,(int) (10 * scale + 0.5f)).height);
        barimg.setLayoutParams(layoutParamsbarimg);
        list.addView(barimg);
    }
    public RelativeLayout MakeColorElement(colourobject colorobj) {
        final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
        final Random rand = new Random();
        RelativeLayout lnl = new RelativeLayout(this);

        RelativeLayout.LayoutParams mainlayout = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lnl.setLayoutParams(mainlayout);
        //lnl.setBackgroundColor(Color.BLACK);


        RelativeLayout colorsview = new RelativeLayout(this);
        RelativeLayout.LayoutParams colorsviewparam = new RelativeLayout.LayoutParams(new GridView.LayoutParams((int) (70 * scale + 0.5f), (int) (70 * scale + 0.5f)));
        colorsviewparam.addRule(RelativeLayout.CENTER_HORIZONTAL);
        colorsviewparam.setMargins(0, (int) (10 * scale + 0.5f), 0, 0);
        colorsview.setLayoutParams(colorsviewparam);
        //colorsview.setBackgroundColor(Color.rgb(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255)));
        CircleImageView cir = new CircleImageView(this);
        Bitmap image = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
        image.eraseColor(Color.parseColor(colorobj.hex));
        cir.setBorderColor(Color.parseColor("#424242"));
        cir.setBorderWidth(8);
        cir.setImageBitmap(image);
        colorsview.addView(cir);
        lnl.addView(colorsview);


        ImageView backabcode = new ImageView(this);
        backabcode.setImageResource(R.drawable.colourgallery_backcode);
        RelativeLayout.LayoutParams backcodeparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        backcodeparams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        backcodeparams.setMargins(0, (int) (65 * scale + 0.5f), 0, 0);
        backabcode.setLayoutParams(backcodeparams);
        lnl.addView(backabcode);

        ImageView backabailable = new ImageView(this);
        backabailable.setImageResource(R.drawable.colourgallery_backavailable);
        RelativeLayout.LayoutParams backparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        backparams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        backparams.setMargins(0, (int) (120 * scale + 0.5f), 0, 0);
        backabailable.setLayoutParams(backparams);
        lnl.addView(backabailable);


        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/BArshia.ttf");
        TextView colorcode = new TextView(this);
        colorcode.setTypeface(type);
        colorcode.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.colournamefontsize));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        params.setMargins(0, (int) (85 * scale + 0.5f), 0, 0);
        colorcode.setLayoutParams(params);
        colorcode.setText("کد رنگ: " + colorobj.code);
        colorcode.setTextColor(Color.rgb(40, 98, 103));
        lnl.addView(colorcode);

        TextView available = new TextView(this);
        available.setTypeface(type);
        available.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.colournamefontsize));
        RelativeLayout.LayoutParams params22 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params22.addRule(RelativeLayout.CENTER_HORIZONTAL);
        params22.setMargins(0, (int) (119 * scale + 0.5f), 0, 0);
        available.setLayoutParams(params22);
        if (colorobj.Available == 1) {
            available.setText("موجود");
            available.setTextColor(Color.WHITE);
        }
        else {
            available.setText("ناموجود");
            available.setTextColor(Color.WHITE);
        }
        lnl.addView(available);
        lnl.setId(colorobj.id);
        lnl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ColourGallery.this, colourdialoge.class);
                myIntent.putExtra("id", v.getId()); //Optional parameters
                ColourGallery.this.startActivity(myIntent);
            }
        });




        return lnl;
    }
}
