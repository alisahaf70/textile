package colour.textile.com.textile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.graphics.Color;
import android.hardware.Camera;
import android.media.MediaActionSound;
import android.os.Bundle;
import android.os.CountDownTimer;

import android.support.v7.graphics.Palette;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CameraActivity extends Activity {

    private Camera mCamera;
    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    private Context appcontext;
    private TextView[] txtv;
    private ArrayList<colourobject> colorarray;
    private ArrayList<colourobject> bestcolor;
    private boolean capturecolor;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        // Create an instance of Camera
        mCamera = getCameraInstance();
        colorarray=DataBaseHelper.getInstance(this).ReadAllColour();
        bestcolor=new ArrayList<colourobject>();
        Camera.Parameters param=mCamera.getParameters();
        List<Camera.Size> sizes=param.getSupportedPictureSizes();
        param.setPictureSize(sizes.get(sizes.size()-1).width,sizes.get(sizes.size()-1).height);
        mCamera.setParameters(param);
        mCamera.setDisplayOrientation(90);
        appcontext=getBaseContext();
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
        //mCamera.takePicture(null,null,mPicture);
        capturecolor=false;
        mPicture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            }

        };
        txtv= new TextView[6];
        txtv[0]=((TextView)findViewById((R.id.t1)));
        txtv[1]=((TextView)findViewById((R.id.t2)));
        txtv[2]=((TextView)findViewById((R.id.t3)));
        txtv[3]=((TextView)findViewById((R.id.t4)));
        txtv[4]=((TextView)findViewById((R.id.t5)));
        txtv[5]=((TextView)findViewById((R.id.t6)));

        ImageButton camerabtn=(ImageButton)(findViewById(R.id.camerabtn));
        camerabtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturecolor=true;
            }
        });
    }
    public void Showcolorpage()
    {
        if (capturecolor==false)
            return;
        capturecolor=false;
        //mCamera.stopPreview();
        DataBaseHelper.getInstance(this).cameracolor=bestcolor;
        Intent myIntent = new Intent(CameraActivity.this, colourdialoge.class);
        myIntent.putExtra("last", "camera"); //Optional parameters
        myIntent.putExtra("id", bestcolor.get(0).id);
        CameraActivity.this.startActivity(myIntent);
        finish();
    }
    public void GetColours(Bitmap bitmap) {
        if (capturecolor)
            Showcolorpage();
        Palette p = Palette.from(bitmap).maximumColorCount(6).generate();
        List<Palette.Swatch> ss = p.getSwatches();
        if (bestcolor.size() > 0)
            bestcolor.clear();
        for (int i = 0; i < 6; i++) {
            if (i < ss.size()) {
                int minval = 9999;
                int tempdist;
                colourobject temp = null;
                for (int j = 0; j < colorarray.size(); j++) {
                    int coloro = Color.parseColor(colorarray.get(j).hex);
                    tempdist = Distance(coloro, ss.get(i).getRgb());
                    if (tempdist < minval && !bestcolor.contains(colorarray.get(j)) ) {
                        minval = tempdist;
                        temp = colorarray.get(j);
                    }
                }
                if (minval < 125) {
                    bestcolor.add(temp);
                }
            }
            txtv[i].setBackgroundColor(Color.TRANSPARENT);
            txtv[i].setVisibility(View.GONE);
        }
        for (int i = 0; i < bestcolor.size(); i++)
        {
            txtv[i].setVisibility(View.VISIBLE);
            txtv[i].setBackgroundColor(Color.parseColor(bestcolor.get(i).hex));
        }
        if (capturecolor)
            Showcolorpage();
        }
    // closed match in RGB space
    public int Distance(int a, int b) {
        return Math.abs(Color.red(a) - Color.red(b)) + Math.abs(Color.green(a)- Color.green(b)) + Math.abs(Color.blue(a) - Color.blue(b));
    }
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

}