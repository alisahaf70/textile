package colour.textile.com.textile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import colour.textile.com.shirwa.simplistic_rss.RssItem;
import colour.textile.com.shirwa.simplistic_rss.RssReader;


public class news extends AppCompatActivity {
    LinearLayout layout;
    JSONArray newsimage = null;
    AsyncTask NewsGetter=null;
    int counter=0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        layout = (LinearLayout) findViewById(R.id.list);
        newsimage = new JSONArray();
        NewsGetter=new GetRssFeed(this).execute("http://nikriss.cloudsite.ir/wordpress/feed/");
    }
    public void SaveImage(Bitmap bigimage)
    {
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut = null;
        File file = new File(path, "thumbnail"+String.valueOf(counter)+".jpg"); // the File to save to
        counter=counter+1;
        try {
            fOut = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bigimage.compress(Bitmap.CompressFormat.JPEG, 85, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
        try {
            fOut.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fOut.close(); // do not forget to close the stream
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            MediaStore.Images.Media.insertImage(getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        if (NewsGetter!=null) {
            NewsGetter.cancel(true);
        }
        super.onBackPressed();
    }
    private class GetRssFeed extends AsyncTask<String, Void, Void> {
        Context context;
        List<RssItem> rssobjects=null;
        ArrayList<RelativeLayout> layoutlist;
        public GetRssFeed(Context callerclass)
        {
            context = callerclass;
            layoutlist=new ArrayList<RelativeLayout>();
        }
        @Override
        protected Void doInBackground(String... params) {
            try {
                RssReader rssReader = new RssReader(params[0]);
                rssobjects= rssReader.getItems();

                for (int i=0;i<Math.min(rssobjects.size(),10);i++)
                    SetupElement(rssobjects.get(i),i);

                SharedPreferences prefs = getSharedPreferences(
                        "colour.textile.com.textile", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                String bbb=new JSONObject().put("item",newsimage).toString();
                editor.putString("newsimage", bbb);
                editor.apply();
                editor.commit();

            } catch (Exception e) {
                Log.v("Error Parsing Data", e + "");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ((news) context).layout.removeAllViews();
            for (int i=0;i<layoutlist.size();i++)
                ((news) context).layout.addView(layoutlist.get(i));
        }
        private void SetupElement(RssItem rss,int index)
        {
            final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
            RelativeLayout lnl=new RelativeLayout(context);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, new GridView.LayoutParams(65, (int) (90 * scale + 0.5f)).height);
            lnl.setLayoutParams(layoutParams2);

            InputStream istr = null;
            try {
                istr = getAssets().open("newsback.png");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap backimage = BitmapFactory.decodeStream(istr);

            InputStream input = null;
            Bitmap bigimage=null;
            try {
                input = new java.net.URL(rss.getImageUrl()).openStream();
            } catch (IOException e) {
                try {
                    istr = getAssets().open("textile.png");
                    bigimage = BitmapFactory.decodeStream(istr);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                e.printStackTrace();
            }
            if (bigimage==null) {
                bigimage = BitmapFactory.decodeStream(input);
                JSONObject inj=new JSONObject();
                try {
                    inj.put("Title",rss.getTitle());
                    inj.put("img", "thumbnail"+String.valueOf(counter)+".jpg");
                    newsimage.put(inj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Bitmap bmp2 = bigimage.copy(bigimage.getConfig(), true);
                ((news)context).SaveImage(getResizedBitmap(bmp2,850,850));
            }
            bigimage=getCroppedBitmap(bigimage);
            int bitmap1Width = backimage.getWidth();
            int bitmap1Height = backimage.getHeight();
            int bitmap2Width = bigimage.getWidth();
            int bitmap2Height = bigimage.getHeight();
            float marginLeft = (float) (bitmap1Width - bitmap2Width +0*bitmap1Width);
            float marginTop = (float) (bitmap1Height * 0.5 - bitmap2Height * 0.5- 0*bitmap1Height);
            Bitmap result = Bitmap.createBitmap(backimage.getWidth(), backimage.getHeight(), backimage.getConfig());
            Canvas canvas = new Canvas(result);
            canvas.drawBitmap(backimage, new Matrix(), null);
            canvas.drawBitmap(bigimage, marginLeft, marginTop, null);



            //Text
            Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
            textPaint.setColor(Color.BLACK);
            textPaint.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics()));
            textPaint.setTextAlign(Paint.Align.RIGHT);
            Typeface type = Typeface.createFromAsset(getAssets(),"fonts/BArshia.ttf");
            textPaint.setTypeface(type);


            int x = ((int)(bitmap1Width - bitmap2Width -0.02*bitmap1Width));
            int y = (int) (bitmap1Height - bitmap2Height * 0.5);
            canvas.drawText(rss.getTitle(),  x , y , textPaint);



            ImageView backimg=new ImageView(context);
            backimg.setImageBitmap(result);
            RelativeLayout.LayoutParams layoutParams4=new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, (int) (100 * scale + 0.5f));
            layoutParams4.addRule(RelativeLayout.CENTER_IN_PARENT);
            backimg.setLayoutParams(layoutParams4);

            lnl.setId(index);
            lnl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url=rssobjects.get(v.getId()).getLink();
                    if (!url.startsWith("http://") && !url.startsWith("https://"))
                        url = "http://" + url;
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                }
            });
            lnl.addView(backimg);
            layoutlist.add(lnl);
        }
        public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
            int width = bm.getWidth();
            int height = bm.getHeight();
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;
            // CREATE A MATRIX FOR THE MANIPULATION
            Matrix matrix = new Matrix();
            // RESIZE THE BIT MAP
            matrix.postScale(scaleWidth, scaleHeight);

            // "RECREATE" THE NEW BITMAP
            Bitmap resizedBitmap = Bitmap.createBitmap(
                    bm, 0, 0, width, height, matrix, false);
            //bm.recycle();
            return resizedBitmap;
        }
        public Bitmap getCroppedBitmap(Bitmap bitmap) {
            bitmap=getResizedBitmap(bitmap,150,150);
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
            canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                    bitmap.getWidth() / 2, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
            //return _bmp;
            return output;
        }
    }
}
