package colour.textile.com.textile;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class textiledialog extends AppCompatActivity {

    TextView txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_textiledialog);
        txt=(TextView)findViewById(R.id.text);
        int id = getIntent().getIntExtra("id",0);
        TextileObject t= DataBaseHelper.getInstance(this).ReadTextile(id);
        setupElement(t);
    }
    private void setupElement(TextileObject t)
    {
        txt.setText("نام: "+t.name+"\n"+"رنگ: "+t.color+"\n"+"توضیحات: "+t.description);
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/BArshia.ttf");
        txt.setTypeface(type);
        txt.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.textilenamefontsize));


        Bitmap bigimage = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(t.thumbnail1, null, getPackageName()));
        bigimage=getCroppedBitmap(bigimage);


        ImageView backimg=(ImageView)findViewById(R.id.imageView1);
        backimg.setImageBitmap(bigimage);
    }
    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        //bitmap=getResizedBitmap(bitmap,200,200);
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

}
