package colour.textile.com.textile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static android.R.attr.defaultValue;

public class MainActivity extends AppCompatActivity  implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{
    int DATABASE_VERSION=21;
    SliderLayout news=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView DetectclrBtn=(ImageView) findViewById(R.id.colordetect);
        ImageView GalleryBtn=(ImageView) findViewById(R.id.textilebtn);
        ImageView ColourBtn=(ImageView) findViewById(R.id.colorGallery);
        ImageView AboutBtn=(ImageView) findViewById(R.id.contactus);
        news=(SliderLayout) findViewById(R.id.news);
        DetectclrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCameraActivity();
            }
        });
        GalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, Textile_gallery.class);
                myIntent.putExtra("key", "hi"); //Optional parameters
                MainActivity.this.startActivity(myIntent);
            }
        });
        ColourBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, ColourGallery.class);
                myIntent.putExtra("last", "main"); //Optional parameters
                MainActivity.this.startActivity(myIntent);
            }
        });
        AboutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, about.class);
                myIntent.putExtra("last", "main"); //Optional parameters
                MainActivity.this.startActivity(myIntent);
            }
        });
        //news.setImageBitmap(opennewsImage());
        SetupSlider();
        OpenDatabase();
        if (isNetworkAvailable())
            checkforupdate();

    }
    public void SetupSlider()
    {
        HashMap<String,File> file_maps = new HashMap<String, File>();
        String path = Environment.getExternalStorageDirectory().toString();

        SharedPreferences prefs = getSharedPreferences(
                "colour.textile.com.textile", Context.MODE_PRIVATE);
        String jsonstr = prefs.getString("newsimage", "{\"item\":[{\"Title\":\"اخبار مربوط به شرکت نیکریس\",\"img\":\"thumbnail.jpg\"}]}");

        JSONObject jsonResponse= null;
        try {
            jsonResponse = new JSONObject(jsonstr);
            JSONArray cast = jsonResponse.getJSONArray("item");
            for (int i=0; i<cast.length(); i++) {
                JSONObject obj = cast.getJSONObject(i);
                String Title = obj.getString("Title");
                String add = obj.getString("img");
                File f = new File(path, add);
                file_maps.put(Title,f);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(this);;

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            news.addSlider(textSliderView);
        }
        news.setPresetTransformer(SliderLayout.Transformer.Accordion);
        news.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        news.setCustomAnimation(new DescriptionAnimation());
        news.setDuration(4000);
        news.addOnPageChangeListener(this);
    }
    public Bitmap opennewsImage()
    {
        InputStream istr = null;
        try {
            istr = getAssets().open("main_news.png");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap backimage = BitmapFactory.decodeStream(istr);
        String path = Environment.getExternalStorageDirectory().toString();
        File f = new File(path, "thumbnail.jpg");
        Bitmap second = null;
        try {
            second = BitmapFactory.decodeStream(new FileInputStream(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //second=getCroppedBitmap(second);
        int bitmap1Width = backimage.getWidth();
        int bitmap1Height = backimage.getHeight();
        int bitmap2Width = second.getWidth();
        int bitmap2Height = second.getHeight();
        float marginLeft = (float) (bitmap1Width*0.5 - bitmap2Width*0.5-0.012*bitmap1Width );
        float marginTop = (float) (bitmap1Height * 0.5 - bitmap2Height * 0.5+ 0.03*bitmap1Height);
        Bitmap result = Bitmap.createBitmap(backimage.getWidth(), backimage.getHeight(), backimage.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(backimage, new Matrix(), null);
        canvas.drawBitmap(second, marginLeft, marginTop, null);

        return result;
    }
    public void OpenDatabase()
    {
        DataBaseHelper db=DataBaseHelper.getInstance(this);
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        int databaseversion = sharedPref.getInt("DATABASE", 0);
        if (databaseversion<DATABASE_VERSION)
        {
            try {
                db.createDataBase();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("DATABASE",DATABASE_VERSION);
                editor.commit();
                db.openDataBase();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
            db.openDataBase();
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public void openCameraActivity()
    {
        if (!checkCameraHardware(this.getApplicationContext()))
            return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasCamera = checkSelfPermission("android.permission.CAMERA");
            if (hasCamera != -1 ) {
                ShowCameraActivity();
            }
            else
            {
                String[] perms = {"android.permission.CAMERA"};
                int permsRequestCode = 200;
                requestPermissions(perms, permsRequestCode);
            }
        } else
            ShowCameraActivity();
    }
    public void ShowCameraActivity()
    {
        Intent myIntent = new Intent(MainActivity.this, CameraActivity.class);
        myIntent.putExtra("key", "hi"); //Optional parameters
        MainActivity.this.startActivity(myIntent);
    }
    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){
        switch(permsRequestCode){
            case 200:
                boolean hasCamera = grantResults[0]== PackageManager.PERMISSION_GRANTED;
                if(hasCamera)
                {
                    ShowCameraActivity();
                }
                else
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        Toast.makeText(this, "برای اجرای این قسمت لطفا دسترسی لازم را به برنامه بدهید", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }
    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }
    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        news.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        if (isNetworkAvailable()) {
            Intent myIntent = new Intent(MainActivity.this, news.class);
            myIntent.putExtra("key", "hi"); //Optional parameters
            MainActivity.this.startActivity(myIntent);
        }
        else
        {
            Toast.makeText(MainActivity.this, "خطا در دسترسی به اینترنت", Toast.LENGTH_LONG).show();
        }
    }
    public void checkforupdate()
    {
        new Thread(new Runnable(){

            public void run(){


                final ArrayList<String> urls=new ArrayList<String>(); //to read each line
                //TextView t; //to show the result, please declare and find it inside onCreate()



                try {
                    // Create a URL for the desired page
                    URL url = new URL("http://nikriss.cloudsite.ir/updateversion.txt"); //My text file location
                    //First open the connection
                    HttpURLConnection conn=(HttpURLConnection) url.openConnection();
                    conn.setConnectTimeout(6000); // timing out in a minute

                    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    //t=(TextView)findViewById(R.id.TextView1); // ideally do this in onCreate()
                    String str;
                    while ((str = in.readLine()) != null) {
                        urls.add(str);
                    }
                    in.close();
                } catch (Exception e) {
                    Log.d("MyTag",e.toString());
                }

                //since we are in background thread, to post results we have to go back to ui thread. do the following for that

                MainActivity.this.runOnUiThread(new Runnable(){
                    public void run(){
                        if (urls.size()==0)
                            return;
                        String a=urls.get(0);
                        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                        int databaseversion = sharedPref.getInt("DATABASE", 0);
                        if (databaseversion<Integer.valueOf(a))
                            Toast.makeText(MainActivity.this, "نسخه جدید از نرم افزار ارائه شده است لطفا آن را بروزرسانی نمایید", Toast.LENGTH_LONG).show();
                    }
                });

            }
        }).start();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (isNetworkAvailable()) {
            Intent myIntent = new Intent(MainActivity.this, news.class);
            myIntent.putExtra("key", "hi"); //Optional parameters
            MainActivity.this.startActivity(myIntent);
        }
        else
        {
            Toast.makeText(MainActivity.this, "خطا در دسترسی به اینترنت", Toast.LENGTH_LONG).show();
        }
        return true;
    }
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {}
}
