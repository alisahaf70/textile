package colour.textile.com.textile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.RelativeLayout;

import android.widget.TextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class Textile_gallery extends Activity {

    /** Duration of wait **/
    private LinearLayout list;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_textile_gallery);
        list = (LinearLayout) findViewById(R.id.list);
        ArrayList<TextileObject> textiles=DataBaseHelper.getInstance(this).ReadAllTextile();
        for (int i=0;i<textiles.size();i++)
            SetupElements(textiles.get(i));
    }
    private void SetupElements(TextileObject textile)
    {
        final float scale = getApplicationContext().getResources().getDisplayMetrics().density;
        RelativeLayout lnl=new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, new GridView.LayoutParams(65, (int) (90 * scale + 0.5f)).height);
        lnl.setLayoutParams(layoutParams2);


        Bitmap backimage = BitmapFactory.decodeResource(getResources(), R.drawable.textilegallery_item);
        Bitmap bigimage = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(textile.thumbnail1, null, getPackageName()));
        Bitmap smallImage = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(textile.thumbnail2, null, getPackageName()));


        bigimage=getCroppedBitmap(bigimage);
        smallImage=getCroppedBitmap(smallImage);
        int bitmap1Width = backimage.getWidth();
        int bitmap1Height = backimage.getHeight();
        int bitmap2Width = bigimage.getWidth();
        int bitmap2Height = bigimage.getHeight();
        float marginLeft = (float) (bitmap1Width - bitmap2Width -0.014*bitmap1Width);
        float marginTop = (float) (bitmap1Height * 0.5 - bitmap2Height * 0.5- 0.16*bitmap1Height);
        Bitmap result = Bitmap.createBitmap(backimage.getWidth(), backimage.getHeight(), backimage.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(backimage, new Matrix(), null);
        canvas.drawBitmap(bigimage, marginLeft, marginTop, null);
        bitmap2Width = smallImage.getWidth();
        bitmap2Height = smallImage.getHeight();
        marginLeft = (float) (bitmap1Width - bitmap2Width -0.19*bitmap1Width);
        marginTop = (float) (bitmap1Height * 0.5 - bitmap2Height * 0.5+ 0.227*bitmap1Height);
        canvas.drawBitmap(smallImage, marginLeft, marginTop, null);


        //Text
        Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));
        textPaint.setTextAlign(Paint.Align.RIGHT);
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/BArshia.ttf");
        textPaint.setTypeface(type);


        int x = ((int)(bitmap1Width - bitmap2Width -0.1*bitmap1Width));
        int y = (int) (bitmap1Height * 0.5 - bitmap2Height * 0.5+ 0.12*bitmap1Height);
        canvas.drawText(textile.description,  x , y , textPaint);

        String show="ُنام طرح: "+textile.name+"  "+textile.color;
        x = ((int)(bitmap1Width - bitmap2Width -0.22*bitmap1Width));
        y = (int) (bitmap1Height * 0.5 - bitmap2Height * 0.5+ 0.55*bitmap1Height);
        canvas.drawText(show,  x , y , textPaint);


        ImageView backimg=new ImageView(this);
        backimg.setImageBitmap(result);
        RelativeLayout.LayoutParams layoutParams4=new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, (int) (100 * scale + 0.5f));
        layoutParams4.addRule(RelativeLayout.CENTER_IN_PARENT);
        backimg.setLayoutParams(layoutParams4);


        lnl.setId(textile.id);
        lnl.addView(backimg);
        lnl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Textile_gallery.this, textiledialog.class);
                myIntent.putExtra("id", v.getId()); //Optional parameters
                Textile_gallery.this.startActivity(myIntent);
            }
        });
        list.addView(lnl);
    }
    public static float pxFromDp(float dp, Context mContext) {
        return dp * mContext.getResources().getDisplayMetrics().density;
    }
    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }
    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        //bitmap=getResizedBitmap(bitmap,200,200);
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(new RectF(0, 100, 100, 200), 10, 10, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }
}