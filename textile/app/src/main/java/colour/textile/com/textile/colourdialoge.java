package colour.textile.com.textile;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class colourdialoge extends AppCompatActivity {

    TextView txt;
    private TextView[] txtv;
    private ArrayList<colourobject> colorarray;
    private ArrayList<colourobject> bestcolor;
    private boolean fromcamera=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colourdialoge);
        colorarray=DataBaseHelper.getInstance(this).ReadAllColour();
        bestcolor=new ArrayList<colourobject>();
        txt=(TextView)findViewById(R.id.text);
        txtv= new TextView[6];
        txtv[0]=((TextView)findViewById((R.id.t1)));
        txtv[1]=((TextView)findViewById((R.id.t2)));
        txtv[2]=((TextView)findViewById((R.id.t3)));
        txtv[3]=((TextView)findViewById((R.id.t4)));
        txtv[4]=((TextView)findViewById((R.id.t5)));
        txtv[5]=((TextView)findViewById((R.id.t6)));
        for(int i = 0; i < 6; i++)
        {
            txtv[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    colourobject t= DataBaseHelper.getInstance(colourdialoge.this).ReadColour(v.getId());
                    setupElement(t);
                    if (fromcamera!=true)
                        GetColours(t);
                }
            });
        }


        int id = getIntent().getIntExtra("id",0);
        colourobject t= DataBaseHelper.getInstance(this).ReadColour(id);

        String last = getIntent().getStringExtra("last");
        setupElement(t);
        if (last==null)
            GetColours(t);
        else if (last.equals("camera")) {
            fromcamera=true;
            setupFromCamera();
        }
    }
    private void setupElement(colourobject t)
    {
        txt.setText("کد: "+t.code+"\n"+"نام رنگ: "+t.name+"\n"+"توضیحات: "+t.Description);
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/BArshia.ttf");
        txt.setTypeface(type);
        txt.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.textilenamefontsize));


        CircleImageView cir = (CircleImageView)findViewById(R.id.profile_image);
        Bitmap image = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
        image.eraseColor(Color.parseColor(t.hex));
        cir.setBorderColor(Color.parseColor("#424242"));
        cir.setBorderWidth(8);
        cir.setImageBitmap(image);
    }

    public void GetColours(colourobject t) {

        if (bestcolor.size() > 0)
            bestcolor.clear();
        for (int i = 0; i < 6; i++) {
                int minval = 9999;
                int tempdist;
                colourobject temp = null;
                for (int j = 0; j < colorarray.size(); j++) {
                    int coloro = Color.parseColor(colorarray.get(j).hex);
                    tempdist = Distance(coloro, Color.parseColor(t.hex));
                    if (tempdist < minval && !bestcolor.contains(colorarray.get(j)) ) {
                        minval = tempdist;
                        temp = colorarray.get(j);
                    }
                }
                if (minval < 200) {
                    bestcolor.add(temp);
                }
            txtv[i].setBackgroundColor(Color.TRANSPARENT);
            txtv[i].setVisibility(View.GONE);
         }
        for(int i = 0; i < bestcolor.size(); i++)
        {
            txtv[i].setVisibility(View.VISIBLE);
            txtv[i].setBackgroundColor(Color.parseColor(bestcolor.get(i).hex));
            txtv[i].setId(bestcolor.get(i).id);
        }
    }
    private void setupFromCamera()
    {
        bestcolor = DataBaseHelper.getInstance(this).cameracolor;
        for (int i=0;i<6;i++)
        {
            txtv[i].setBackgroundColor(Color.TRANSPARENT);
            txtv[i].setVisibility(View.GONE);
        }
        for(int i = 0; i < bestcolor.size(); i++)
        {
            txtv[i].setVisibility(View.VISIBLE);
            txtv[i].setBackgroundColor(Color.parseColor(bestcolor.get(i).hex));
            txtv[i].setId(bestcolor.get(i).id);
        }
    }
    public int Distance(int a, int b) {
        return Math.abs(Color.red(a) - Color.red(b)) + Math.abs(Color.green(a)- Color.green(b)) + Math.abs(Color.blue(a) - Color.blue(b));
    }
}
