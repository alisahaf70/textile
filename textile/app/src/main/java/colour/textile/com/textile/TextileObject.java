package colour.textile.com.textile;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.camera2.CameraManager;
import android.util.Base64;

/**
 * Created by Alireza on 04/06/2017.
 */

public class TextileObject {
    String  thumbnail1,thumbnail2;
    String name,color,description;
    int Available,id;
    public TextileObject(int _id,String _name,String _color,String _base1,int _Available,String _description,String _Base2){
        id=_id;
        name=_name;
        color=_color;
        Available= _Available;
        description=_description;
        thumbnail1="@drawable/"+_base1;
        thumbnail2="@drawable/"+_Base2;
    }
}
